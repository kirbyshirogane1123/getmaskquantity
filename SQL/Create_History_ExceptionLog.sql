USE [MainDB]
GO

/****** Object:  Table [dbo].[History_ExceptionLog]    Script Date: 2020/5/20 �W�� 07:10:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[History_ExceptionLog](
	[GUID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ErrorProcedure] [varchar](100) NOT NULL,
	[ErrorNumber] [int] NOT NULL,
	[ErrorMessage] [nvarchar](4000) NOT NULL,
	[ErrorSeverity] [int] NOT NULL,
	[ErrorState] [int] NOT NULL,
	[ErrorLine] [int] NOT NULL,
	[InputValue] [nvarchar](1000) NOT NULL,
	[CreateTime] [datetime] NOT NULL CONSTRAINT [DF_History_ExceptionLog_CreateTime]  DEFAULT (getdate()),
 CONSTRAINT [PK_History_ExceptionLog] PRIMARY KEY CLUSTERED 
(
	[GUID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


