USE [MainDB]
GO

/****** Object:  StoredProcedure [dbo].[usp_GD_Credit_Create]    Script Date: 2020/5/20 上午 07:57:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JeffZhang
-- Create date: 2020/05/20
-- Description:	經Check後將沒存在表內的資料存至TW_MaskQuantity
/*
	DECLARE @strResult VARCHAR(10)
	EXECUTE [[SP_MaskQuantiy_Insert]]  
			@HOSP_ID			  = '0123456789'  
			@HOSP_Name			  = N'台北測試醫院'
			@HOSP_Address		  = N'新北市TEST村２１７號'
			@HOSP_Phone			  = '623995'
			@AdultMaskQuantity	  = '2880'
			@ChildMaskQuantity	  = '200'
			@UpdateTime		      = '2020/05/19 12:08:32'
			@strResult = @strResult OUTPUT
	SELECT @strResult
*/


CREATE PROCEDURE [dbo].[SP_MaskQuantiy_Data_Create]
	@HOSP_ID		   varchar(10),				   --醫事機構代碼
	@HOSP_Name	       nvarchar(30),			   --醫事機構名稱
	@HOSP_Address      nvarchar (50),  		       --醫事機構地址
	@HOSP_Phone        nvarchar (15),		       --醫事機構電話
	@AdultMaskQuantity varchar(10),                --成人口罩剩餘數
	@ChildMaskQuantity varchar(10),	               --兒童口罩剩餘數
	@UpdateTime        datetime, 		  	       --來源資料時間
	@strResult	       VARCHAR(10) OUT             /*輸出的執行結果代碼*/
	--雖確定是JSON抓進來 但怕還是有意外 故先用Varchar塞進來 非英數字源一樣使用nvarchar
AS
BEGIN
	BEGIN TRY

		SET nocount ON

		BEGIN TRANSACTION
			INSERT INTO [dbo].[TW_MaskQuantity]
			(
				HOSP_ID,     
				HOSP_Name,
				HOSP_Address,
				HOSP_Phone,    
				AdultMaskQuantity,
				ChildMaskQuantity,
				UpdateTime,       
				CreateTime       
			)
			VALUES
			(
				@HOSP_ID,
				@HOSP_Name,    
				@HOSP_Address,
				@HOSP_Phone,
				@AdultMaskQuantity,
				@ChildMaskQuantity,
				@UpdateTime,
				GETDATE()
			 )
			IF @@ROWCOUNT <> 1
			BEGIN
				SET @strResult = '0.99'      
				PRINT '資料異常'
				ROLLBACK TRANSACTION
				RETURN
			END

		COMMIT TRANSACTION
		SET @strResult = '1.0'
		RETURN

	END TRY
	BEGIN CATCH 
      DECLARE @strErrorProcedure NVARCHAR(100) = Error_procedure() 
      DECLARE @intErrorNumber INT = Error_number() 
      DECLARE @strErrorMessage NVARCHAR(4000) = Error_message() 
      DECLARE @intErrorSeverity INT = Error_severity() 
      DECLARE @intErrorState INT = Error_state() 
      DECLARE @intErrorLine INT = Error_line() 
      DECLARE @strInputValue NVARCHAR(1000) = CONVERT(VARCHAR, '') + ';'

      PRINT @strErrorMessage 

      --寫入例外錯誤紀錄 
      EXEC Ssp_history_exceptionlog_create 
        @strErrorProcedure, 
        @intErrorNumber, 
        @strErrorMessage, 
        @intErrorSeverity, 
        @intErrorState, 
        @intErrorLine, 
        @strInputValue 

      SET @strResult = '-1.0' 
  END CATCH

END

GO

