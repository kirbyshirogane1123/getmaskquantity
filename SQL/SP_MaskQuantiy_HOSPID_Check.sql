USE [MainDB]
GO

/****** Object:  StoredProcedure [dbo].[SP_MaskQuantiy_HOSPID_Check]    Script Date: 2020/5/25 上午 07:45:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		JeffZhang
-- Create date: 2020-05-19
-- Description:	檢查醫事機構代碼是否在表裡面
-- ============================================
/*
傳入參數
  @strHOSP_ID char(10)

執行範例

	DECLARE @strResult VARCHAR(10)
	EXECUTE [SP_MaskQuantiy_HOSPID_Check]  
			@strHOSP_ID = '0145080011',
			@strResult  OUT
	SELECT @strResult
*/

CREATE PROCEDURE [dbo].[SP_MaskQuantiy_HOSPID_Check]
(	@strHOSP_ID			CHAR(10)			--醫事機構代碼
,	@strResult			VARCHAR (10)	OUT --回傳代碼
)
AS 
	BEGIN 
		Set NoCount On
			BEGIN TRY
			--檢查醫事機構代碼是否在表裡面
				IF EXISTS	
				(
					Select HOSP_ID
					From TW_MaskQuantity with (Nolock)
					Where HOSP_ID = @strHOSP_ID 
				)
			BEGIN
				Set @strResult = '1.0'
				--若在表裡面就回傳1.0 由另外一隻SP做更新 或做其他事情
			End
	ELSE
			BEGIN
				Set @strResult = '0.0'
				--此醫院/藥局不存在
				--若不存再表裡面回傳0.0 
			End
	END TRY
BEGIN CATCH
    DECLARE @strErrorProcedure NVARCHAR(100) = ERROR_PROCEDURE()
    DECLARE @intErrorNumber INT = ERROR_NUMBER()
    DECLARE @strErrorMessage NVARCHAR(4000) = ERROR_MESSAGE()
    DECLARE @intErrorSeverity INT = ERROR_SEVERITY()
    DECLARE @intErrorState INT = ERROR_STATE()
    DECLARE @intErrorLine INT = ERROR_LINE()
    DECLARE @strInputValue NVARCHAR(1000) = CONVERT(VARCHAR, '') + ';'  

    --寫入例外錯誤紀錄
    EXEC ssp_History_ExceptionLog_Create 
	  @strErrorProcedure, @intErrorNumber, 
	  @strErrorMessage, 
	  @intErrorSeverity, 
	  @intErrorState, 
	  @intErrorLine, 
	  @strInputValue

    SET @strResult = '-1.0'
             
END CATCH

END


GO


