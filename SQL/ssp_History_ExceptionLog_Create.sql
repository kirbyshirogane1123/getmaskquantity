USE [MainDB]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author        : JeffZhang
-- Create date   : 2020/05/20
-- Description   : [系統]寫入例外錯誤紀錄

/* 
傳入參數
	@strErrorProcedure	NVARCHAR(100),		-- Stored procedure 名稱
	@intErrorNumber		INT,				-- 錯誤代碼
	@strErrorMessage	NVARCHAR(4000),		-- 錯誤訊息
	@intErrorSeverity	INT,				-- 錯誤等級
	@intErrorState		INT,				-- 錯誤狀態
	@intErrorLine		INT,				-- 出錯行數
	@strInputValue		NVARCHAR(1000)		-- 傳入參數

執行範例
	DECLARE @strResult VARCHAR(10);
	EXECUTE [ssp_History_ExceptionLog_Create]
				@strErrorProcedure = 'Test_SP',
				@intErrorNumber = 1,
				@strErrorMessage = 'This is for testing.',
				@intErrorSeverity = 2,
				@intErrorState = 3,
				@intErrorLine = 4,
				@strInputValue = 'Sample'
	;
*/
-- =============================================
CREATE PROCEDURE [dbo].[ssp_History_ExceptionLog_Create]
	@strErrorProcedure	NVARCHAR(100),		-- Stored procedure 名稱
	@intErrorNumber		INT,				-- 錯誤代碼
	@strErrorMessage	NVARCHAR(4000),		-- 錯誤訊息
	@intErrorSeverity	INT,				-- 錯誤等級
	@intErrorState		INT,				-- 錯誤狀態
	@intErrorLine		INT,				-- 出錯行數
	@strInputValue		NVARCHAR(1000)		-- 傳入參數
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO [History_ExceptionLog]
		(
			[ErrorProcedure],
			[ErrorNumber],
			[ErrorMessage],
			[ErrorSeverity],
			[ErrorState],
			[ErrorLine],
			[InputValue]
		)
		VALUES
		(
			@strErrorProcedure,
			@intErrorNumber,
			@strErrorMessage,
			@intErrorSeverity,
			@intErrorState,
			@intErrorLine,
			@strInputValue
		)
	;
END

GO


