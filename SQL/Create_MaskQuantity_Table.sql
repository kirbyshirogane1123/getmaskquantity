USE MainDB
GO

CREATE TABLE TW_MaskQuantity
(
	GUID INT		  PRIMARY KEY IDENTITY,
	HOSP_ID		      char(10) NOT NULL,           --醫事機構代碼
	HOSP_Name	      nvarchar(30) NOT NULL,       --醫事機構名稱
	HOSP_Address      nvarchar (50),  		       --醫事機構地址
	HOSP_Phone        nchar (15),		           --醫事機構電話
	AdultMaskQuantity nchar(10),                   --成人口罩剩餘數
	ChildMaskQuantity nchar(10),	               --兒童口罩剩餘數
	UpdateTime        datetime, 		  	       --來源資料時間
	CreateTime        datetime					   --資料建立時間
)
GO

--建立Index至醫事機構代碼 以利後續抓資料查詢
CREATE INDEX IDX_HOSP_ID
ON TW_MaskQuantity (HOSP_ID)





