USE [MainDB]
GO

/****** Object:  StoredProcedure [dbo].[SP_MaskQuantiy_Data_MaskQuantity_Update]    Script Date: 2020/5/25 上午 07:44:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		JeffZhang
-- Create date: 2020/05/20
-- Description:	經Check後將存在表內的資料更新上傳時間及口罩數量
/*
	DECLARE @strResult VARCHAR(10)
	EXECUTE [MainDB].[dbo].[SP_MaskQuantiy_Data_MaskQuantity_Update]  
			@HOSP_ID			  = '0123456789',
			@AdultMaskQuantity	  = '2880',
			@ChildMaskQuantity	  = '200',
			@UpdateTime		      = '2020/05/19 12:08:32',
			@strResult = @strResult OUTPUT
	SELECT @strResult
*/


CREATE PROCEDURE [dbo].[SP_MaskQuantiy_Data_MaskQuantity_Update]
	@strHOSP_ID		   varchar(10),				   --醫事機構代碼
	@strAdultMaskQuantity varchar(10),             --成人口罩剩餘數
	@strChildMaskQuantity varchar(10),	           --兒童口罩剩餘數
	@UpdateTime        datetime, 		  	       --來源資料時間
	@strResult	       VARCHAR(10) OUT             /*輸出的執行結果代碼*/
	--因確認後機構代碼為唯一值不會重複，並且table內hosp_ID以有建Index，故只帶機構代碼作為更新依據，
AS
BEGIN
	BEGIN TRY

		SET NOCOUNT ON

		BEGIN TRANSACTION
			UPDATE [dbo].[TW_MaskQuantity]
			SET 
				AdultMaskQuantity = @strAdultMaskQuantity,
				ChildMaskQuantity = @strChildMaskQuantity,
				UpdateTime	      = @UpdateTime,    
				CreateTime		  = GETDATE()
			WHERE
				HOSP_ID			  = @strHOSP_ID 
			IF @@ROWCOUNT <> 1
			   OR @@error <> 0
			BEGIN
				SET @strResult = '0.99'      
				PRINT 'Data Error'
				ROLLBACK TRANSACTION
				RETURN
			END

		COMMIT TRANSACTION
		SET @strResult = '1.0'
		RETURN

	END TRY
	BEGIN CATCH 
      DECLARE @strErrorProcedure NVARCHAR(100) = Error_procedure() 
      DECLARE @intErrorNumber INT = Error_number() 
      DECLARE @strErrorMessage NVARCHAR(4000) = Error_message() 
      DECLARE @intErrorSeverity INT = Error_severity() 
      DECLARE @intErrorState INT = Error_state() 
      DECLARE @intErrorLine INT = Error_line() 
      DECLARE @strInputValue NVARCHAR(1000) = CONVERT(VARCHAR, '') + ';'

      PRINT @strErrorMessage 

      --寫入例外錯誤紀錄 
      EXEC Ssp_history_exceptionlog_create 
        @strErrorProcedure, 
        @intErrorNumber, 
        @strErrorMessage, 
        @intErrorSeverity, 
        @intErrorState, 
        @intErrorLine, 
        @strInputValue 

      SET @strResult = '-1.0' 
  END CATCH

END


GO


